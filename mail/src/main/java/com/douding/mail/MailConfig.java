package com.douding.mail;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author: ding dou
 * @className: MailConfig
 * @packageName: com.douding.mail
 * @description: 邮件配置类
 **/
@Component
public class MailConfig {


    @Bean
    public Queue queue(){
        return new Queue("mail.welcome");
    }

}

