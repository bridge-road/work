package com.douding.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.Department;
import com.douding.server.vo.RespBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IDepartmentService extends IService<Department> {

    /**
     * @description:  获取所有部门
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Department>
     * @title: getAllDepartments
     * @author: ding dou
     *
     */
    List<Department> getAllDepartments();

    /**
     * @description:  添加部门
     *
     * @param dep
     * @return: com.douding.server.vo.RespBean
     * @title: addDept
     * @author: ding dou
     *
     */
    RespBean addDept(Department dep);
    /**
     * @description:  删除部门
     *
     * @param id
     * @return: com.douding.server.vo.RespBean
     * @title: deleteDep
     * @author: ding dou
     *
     */
    RespBean deleteDep(Integer id);
}
