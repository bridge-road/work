package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.NationMapper;
import com.douding.server.pojo.Nation;
import com.douding.server.service.INationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class NationServiceImpl extends ServiceImpl<NationMapper, Nation> implements INationService {

}
