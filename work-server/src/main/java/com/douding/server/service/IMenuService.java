package com.douding.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.Menu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IMenuService extends IService<Menu> {

    /**
     * @description:  根据用户id查询菜单列表
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getMenusByAdminId
     * @author: ding dou
     *
     */
    List<Menu> getMenusByAdminId();

    /**
     * @description:  根据角色获取菜单列表
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getMenusWithRole
     * @author: ding dou
     *
     */
    List<Menu> getMenusWithRole();

    /**
     * @description:  查询所有菜单
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getAllMenus
     * @author: ding dou
     *
     */
    List<Menu> getAllMenus();
}
