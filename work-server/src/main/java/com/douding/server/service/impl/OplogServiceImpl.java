package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.OplogMapper;
import com.douding.server.pojo.Oplog;
import com.douding.server.service.IOplogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class OplogServiceImpl extends ServiceImpl<OplogMapper, Oplog> implements IOplogService {

}
