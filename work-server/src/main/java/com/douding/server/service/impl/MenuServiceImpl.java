package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.MenuMapper;
import com.douding.server.pojo.Admin;
import com.douding.server.pojo.Menu;
import com.douding.server.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<Menu> getMenusByAdminId() {
        Integer id = ((Admin) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        //从redis获取菜单数据
        List<Menu> list=(List<Menu>) valueOperations.get("menu_" + id);
        //如果为空，则从数据库获取
        if(CollectionUtils.isEmpty(list)){
               list= menuMapper.getMenusByAdminId(id);
               //将数据设置到redis中
               valueOperations.set("menu_"+id,list);
            }
        return list;

    }

    @Override
    public List<Menu> getMenusWithRole() {

        return menuMapper.getMenusWithRole();

    }

    @Override
    public List<Menu> getAllMenus() {

        return menuMapper.getAllMenus();

    }
}
