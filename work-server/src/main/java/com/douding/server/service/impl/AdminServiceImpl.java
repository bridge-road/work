package com.douding.server.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.config.security.component.JwtTokenUtil;
import com.douding.server.mapper.AdminMapper;
import com.douding.server.mapper.AdminRoleMapper;
import com.douding.server.mapper.RoleMapper;
import com.douding.server.pojo.Admin;
import com.douding.server.pojo.AdminRole;
import com.douding.server.pojo.Role;
import com.douding.server.service.IAdminService;
import com.douding.server.util.AdminUtil;
import com.douding.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

        @Autowired
        private UserDetailsService userDetailsService;
        @Autowired
        private  PasswordEncoder passwordEncoder;
        @Autowired
        private JwtTokenUtil jwtTokenUtil;
        @Autowired
        private AdminMapper adminMapper;
        @Autowired
        private AdminRoleMapper adminRoleMapper;
        @Value("${jwt.tokenHead}")
        private String tokenHead;

        @Autowired
         private  RoleMapper roleMapper;

        /**
     * @description: 登陆之后返回token
     *
     * @param username
		 * @param password
		 * @param code
         * @param request
* @return: com.douding.server.vo.RespBean
     * @title: login
     * @author: user
     * @throws:
     */
    @Override
    public RespBean login(String username, String password, String code, HttpServletRequest request) {
        String captcha = (String) request.getSession().getAttribute("captcha");
        if (StringUtils.isEmpty(code)||!captcha.equalsIgnoreCase(code)){
            return RespBean.error("验证码输入错误，请重新输入！");
        }
        //登陆
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if(null==userDetails||!passwordEncoder.matches(password, userDetails.getPassword())){
            return RespBean.error("用户名或密码不正确");
        }
        if(!userDetails.isEnabled()){
            return RespBean.error("账号被禁用，请联系管理员");
        }
        //更新security登录用户对象
        UsernamePasswordAuthenticationToken authenticationToken=
                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        //生成token
        String token = jwtTokenUtil.generateToken(userDetails);
        Map<String,String> tokenMap=new HashMap<>();
        tokenMap.put("token",token);
        tokenMap.put("tokenHead",tokenHead);

        return RespBean.success("登陆成功",tokenMap);
    }


    /**
     * @description: 根据用户名获取用户
     *
     * @param username
     * @return: com.douding.server.pojo.Admin
     * @title: getAdminByUserName
     * @author:
     * @throws:
     */
    @Override
    public Admin getAdminByUserName(String username) {
        return adminMapper.selectOne(new QueryWrapper<Admin>().eq("username", username)
                .eq("enabled",true));
    }

    @Override
    public List<Role> getRoles(Integer id) {
      return roleMapper.getRoles(id);

    }

    @Override
    public List<Admin> getAllAdmins(String keywords) {

        return adminMapper.getAllAdmins(AdminUtil.getCurrentAdmin().getId()
        ,keywords);
    }

    @Override
    @Transactional
    public RespBean updateAdminRole(Integer adminId, Integer[] rids) {

            adminRoleMapper.delete(new QueryWrapper<AdminRole>().eq("adminId", adminId));
        Integer result=adminRoleMapper.addAdminRole(adminId,rids);
        if(rids.length==result){
            return RespBean.success("更新成功！！！");
        }
        return RespBean.error("更新失败！！！");

    }


}
