package com.douding.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.Employee;
import com.douding.server.vo.ResPageBean;
import com.douding.server.vo.RespBean;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IEmployeeService extends IService<Employee> {

   /**
    * @description:  分页获取所有员工
    *
    * @param currentPage
		 * @param size
		 * @param employee
    * @return: com.douding.server.vo.RespBean
    * @title: getEmployeeByPage
    * @author: ding dou
    *
    */

    ResPageBean getEmployeeByPage(Integer currentPage, Integer size, Employee employee, LocalDate[] beginDateScope);

    /**
     * @description:  查询员工
     *
     * @param id
     * @return: java.util.List<com.douding.server.pojo.Employee>
     * @title: getEmployee
     * @author: ding dou
     *
     */

    List<Employee> getEmployee(Integer id);
    /**
     * @description:  添加员工
     *
     * @param employee
     * @return: com.douding.server.vo.RespBean
     * @title: addEmp
     * @author: ding dou
     *
     */
    RespBean addEmp(Employee employee);
}
