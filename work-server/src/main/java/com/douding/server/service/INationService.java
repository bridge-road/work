package com.douding.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.Nation;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface INationService extends IService<Nation> {

}
