package com.douding.server.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.MenuRoleMapper;
import com.douding.server.pojo.MenuRole;
import com.douding.server.service.IMenuRoleService;
import com.douding.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class MenuRoleServiceImpl extends ServiceImpl<MenuRoleMapper, MenuRole> implements IMenuRoleService {

    @Autowired
    private MenuRoleMapper menuRoleMapper;
    @Override
    public RespBean updataMenusRole(Integer rid, Integer[] mids) {

    menuRoleMapper.delete(new QueryWrapper<MenuRole>().eq("rid", rid));
    if(null==mids||0==mids.length){
        return RespBean.success("更新成功");
    }
        Integer integer = menuRoleMapper.insertRecord(rid, mids);
    if(integer==mids.length){
        return RespBean.success("更新成功！！！");
    }
    return RespBean.error("更新失败！！！");
    }
}
