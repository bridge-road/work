package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.DepartmentMapper;
import com.douding.server.pojo.Department;
import com.douding.server.service.IDepartmentService;
import com.douding.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> getAllDepartments() {

        return departmentMapper.getAllDepartment(1);
    }

    @Override
    public RespBean addDept(Department dep) {

        dep.setEnabled(true);
        departmentMapper.addDept(dep);

        if(1==dep.getResult()){
            return RespBean.success("添加成功",dep);
        }
        return RespBean.error("添加失败！");


    }

    @Override
    public RespBean deleteDep(Integer id) {

        Department department = new Department();
        department.setId(id);
        departmentMapper.deleDep(department);
        if(-2==department.getResult()){
            return RespBean.error("该部门下还有子部门，删除失败");
        }
        if(-1==department.getResult()){
            return RespBean.error("该部门下还有员工，删除失败！");
        }
        if(1==department.getResult()){
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败！");
    }
}
