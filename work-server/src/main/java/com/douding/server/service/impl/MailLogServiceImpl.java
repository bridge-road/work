package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.MailLogMapper;
import com.douding.server.pojo.MailLog;
import com.douding.server.service.IMailLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class MailLogServiceImpl extends ServiceImpl<MailLogMapper, MailLog> implements IMailLogService {

}
