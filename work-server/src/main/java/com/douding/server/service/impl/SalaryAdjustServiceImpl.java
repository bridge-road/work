package com.douding.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.SalaryAdjustMapper;
import com.douding.server.pojo.SalaryAdjust;
import com.douding.server.service.ISalaryAdjustService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class SalaryAdjustServiceImpl extends ServiceImpl<SalaryAdjustMapper, SalaryAdjust> implements ISalaryAdjustService {

}
