package com.douding.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.Admin;
import com.douding.server.pojo.Role;
import com.douding.server.vo.RespBean;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IAdminService extends IService<Admin> {

    /**
     * @description: 登陆接口
     *
     * @param username
		 * @param password
		 * @param code
     * @param request
* @return: com.douding.server.vo.RespBean
     * @title: login
     * @author: user
     * @throws:
     */
    RespBean login(String username, String password, String code, HttpServletRequest request);

    /**
     * @description: 根据用户名获取用户
     *
     * @param
     * @return: com.douding.server.pojo.Admin
     * @title: getAdminByUserName
     * @author: user
     * @throws:
     */
    Admin getAdminByUserName(String username);
    
    /**
     * @description:  根据用户id查询角色列表
     *     
     * @param id
     * @return: java.util.List<com.douding.server.pojo.Role>
     * @title: getRoles
     * @author: ding dou
     * 
     */   
    List<Role> getRoles(Integer id);
    /**
     * @description:  获取所有操作员
     *     
     * @param keywords
     * @return: java.util.List<com.douding.server.pojo.Admin>
     * @title: getAllAdmins
     * @author: ding dou
     * 
     */   
    List<Admin> getAllAdmins(String keywords);

    /**
     * @description:  更新操作员信息
     *
     * @param adminId
		 * @param rids
     * @return: com.douding.server.vo.RespBean
     * @title: updateAdminRole
     * @author: ding dou
     *
     */

    RespBean updateAdminRole(Integer adminId, Integer[] rids);
}
