package com.douding.server.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douding.server.mapper.EmployeeMapper;
import com.douding.server.pojo.Employee;
import com.douding.server.service.IEmployeeService;
import com.douding.server.vo.ResPageBean;
import com.douding.server.vo.RespBean;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * @param currentPage
     * @param size
     * @param employee
     * @description: 分页获取所有员工
     * @return: com.douding.server.vo.RespBean
     * @title: getEmployeeByPage
     * @author: ding dou
     */

    @Override
    public ResPageBean getEmployeeByPage(Integer currentPage, Integer size, Employee employee,
                                         LocalDate[] beginDateScope) {
        //开启分页
        Page<Employee> page = new Page<>(currentPage, size);
        IPage employeeByPage = employeeMapper.getEmployeeByPage(page, employee, beginDateScope);

        ResPageBean resPageBean = new ResPageBean(employeeByPage.getTotal(), employeeByPage.getRecords());


        return resPageBean;

    }

    /**
     * @param id
     * @description: 查询员工
     * @return: java.util.List<com.douding.server.pojo.Employee>
     * @title: getEmployee
     * @author: ding dou
     */

    @Override
    public List<Employee> getEmployee(Integer id) {

        return employeeMapper.getEmployee(id);
    }

    /**
     * @param employee
     * @description: 添加员工
     * @return: com.douding.server.vo.RespBean
     * @title: addEmp
     * @author: ding dou
     */
    @Override
    public RespBean addEmp(Employee employee) {
        //处理合同期限，保留2位小数
        LocalDate beginContract = employee.getBeginContract();
        LocalDate endContract = employee.getEndContract();
        long days = beginContract.until(endContract, ChronoUnit.DAYS);
        DecimalFormat decimalFormat = new DecimalFormat("##.00");
        employee.setContractTerm(Double.parseDouble(decimalFormat.format(days / 365.00)));
        if (1 == employeeMapper.insert(employee)) {
            Employee emp = employeeMapper.getEmployee(employee.getId()).get(0);
            rabbitTemplate.convertAndSend("mail.welcome", emp);
        return RespBean.success("添加成功！！！！");
        } return RespBean.error("添加失败！！！");
    }
}
