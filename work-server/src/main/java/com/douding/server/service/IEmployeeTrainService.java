package com.douding.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.EmployeeTrain;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IEmployeeTrainService extends IService<EmployeeTrain> {

}
