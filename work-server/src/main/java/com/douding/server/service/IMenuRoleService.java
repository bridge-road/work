package com.douding.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douding.server.pojo.MenuRole;
import com.douding.server.vo.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface IMenuRoleService extends IService<MenuRole> {

    /**
     * @description:  更新角色信息
     *
     * @param rid
		 * @param mids
     * @return: com.douding.server.vo.RespBean
     * @title: updataMenusRole
     * @author: ding dou
     *
     */
    RespBean updataMenusRole(Integer rid, Integer[] mids);
}
