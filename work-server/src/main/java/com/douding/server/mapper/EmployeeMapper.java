package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.douding.server.pojo.Employee;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Repository
public interface EmployeeMapper extends BaseMapper<Employee> {


    /**
     * @description:  获取所有员工（分页）
     *
     * @param page
		 * @param employee
		 * @param beginDateScope
     * @return: com.baomidou.mybatisplus.core.metadata.IPage
     * @title: getEmployeeByPage
     * @author: ding dou
     *
     */

    IPage getEmployeeByPage(Page<Employee> page, @Param("employee") Employee employee, @Param(
            "beginDateScope") LocalDate[] beginDateScope);

    /**
     * @description:  查询员工
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Employee>
     * @title: getEmployee
     * @author: ding dou
     *
     */
    List<Employee> getEmployee(@Param("id") Integer id);
}
