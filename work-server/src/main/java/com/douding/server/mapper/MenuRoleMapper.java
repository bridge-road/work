package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.MenuRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Repository
public interface MenuRoleMapper extends BaseMapper<MenuRole> {
    /**
     * @description:  批量更新菜单
     *
     * @param rid
		 * @param mids
     * @return: java.lang.Integer
     * @title: insertRecord
     * @author: ding dou
     *
     */
    Integer insertRecord(@Param("rid") Integer rid, @Param("mids") Integer[] mids);
}
