package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.EmployeeEc;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface EmployeeEcMapper extends BaseMapper<EmployeeEc> {

}
