package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.PoliticsStatus;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface PoliticsStatusMapper extends BaseMapper<PoliticsStatus> {

}
