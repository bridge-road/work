package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.Menu;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * @description:  根据用户id查询菜单
     *
     * @param id
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getMenusByAdminId
     * @author: ding dou
     *
     */
    List<Menu> getMenusByAdminId(Integer id);

    /**
     * @description:  根据角色获取菜单
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getMenusWithRole
     * @author: ding dou
     *
     */
    List<Menu> getMenusWithRole();

    /**
     * @description:  查询所有菜单
     *
     * @param
     * @return: java.util.List<com.douding.server.pojo.Menu>
     * @title: getAllMenus
     * @author: ding dou
     *
     */
    List<Menu> getAllMenus();
}
