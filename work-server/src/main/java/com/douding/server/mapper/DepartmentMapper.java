package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.Department;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> getAllDepartment(Integer parentId);

    /**
     * @description:  添加部门
     *
     * @param dep
     * @return:
     * @title:
     * @author: ding dou
     *
     */
    void addDept(Department dep);

    /**
     * @description:  删除部门
     *
     * @param department
     * @return: void
     * @title: deleDep
     * @author: ding dou
     *
     */

    void deleDep(Department department);
}
