package com.douding.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.AdminRole;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Repository
public interface AdminRoleMapper extends BaseMapper<AdminRole> {


    Integer addAdminRole(Integer adminId, Integer[] rids);
}
