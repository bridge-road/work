package com.douding.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douding.server.pojo.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {


    List<Admin> getAllAdmins(@Param("id") Integer id, @Param("keywords") String keywords);
}
