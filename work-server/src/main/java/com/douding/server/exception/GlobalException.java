package com.douding.server.exception;

import com.douding.server.vo.RespBean;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author: ding dou
 * @className: GlobalException
 * @packageName: com.douding.server.exception
 * @description: 全局异常处理
 **/
@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler(SQLException.class)
    public RespBean mysqlException(SQLException e){
        if(e instanceof SQLIntegrityConstraintViolationException){
            return RespBean.error("该数据库有关联数据，操作失败");
        }
        return RespBean.error("数据库异常，操作失败！");
    }
}

