package com.douding.server.config.security.component;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: work
 * @Package: com.douding.server.config.security
 * @ClassName: JwtTokenUtil
 * @Author: LZB
 * @Description: JwtToken工具类
 */

@Component
public class JwtTokenUtil {

    private static final String CLAIM_KEY_USERNAME="sub";
    private static final String CLAIM_KEY_CREATED="created";
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expiration;

    /**
     * 功能描述 :根据用户信息生成token
     * @author LZB
     * @param userDetails
     * @return String
     */
    public String generateToken(UserDetails userDetails){
        Map<String, Object> map = new HashMap<>();
        map.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        map.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(map);
    }
    /**
     * 功能描述 :从token中获取登录名
     * @author LZB
     * @param
     * @return
     */
    public String getUserNameFromToken(String token){
        String username;
        try {
            Claims claim=getClaimFromToken(token);
            username=claim.getSubject();
        } catch (Exception e) {
            username=null;
        }
        return username;
    }
    /**
     * 功能描述 :从token中获取荷载
     * @author LZB
     * @param
     * @return
     */
    private Claims getClaimFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            e.printStackTrace();

        }
        return claims;
    }
    /**
     * 功能描述 :验证token是否有效
     * @author LZB
     * @param
     * @return
     */
    public boolean validateToken(String token,UserDetails userDetails){
        String nameFromToken = getUserNameFromToken(token);
        return  nameFromToken.equals(userDetails.getUsername())&&!isTokenExpired(token);
    }
    /**
     * 功能描述 :判断token是否可以被刷新（过期就可以被刷新）
     * @author LZB
     * @param
     * @return
     */
    public  boolean canRefresh(String token){
        return !isTokenExpired(token);
    }

    public String refreshToken(String token){
        Claims claims = getClaimFromToken(token);
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }


    /**
     * 功能描述 :判断token是否失效
     * @author LZB
     * @param
     * @return
     */
    private boolean isTokenExpired(String token) {
     Date expireDate= getExpiredDateFromToken(token);
        return expireDate.before(new Date());
    }
    /**
     * 功能描述 :从token中获取过期时间
     * @author LZB
     * @param
     * @return
     */
    private Date getExpiredDateFromToken(String token) {
        Claims fromToken = getClaimFromToken(token);
        Date expiration = fromToken.getExpiration();
        return expiration;

    }


    /**
     * 功能描述 :根据荷载生成JWT Token
     * @author LZB
     * @param
     * @return
     */
    private String generateToken(Map<String,Object> claim){

        return Jwts.builder()
                .setClaims(claim)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }
/**
 * 功能描述 :s生成Token失效时间
 * @author LZB
 * @param
 * @return
 */
    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis()+expiration*1000);
    }

}
