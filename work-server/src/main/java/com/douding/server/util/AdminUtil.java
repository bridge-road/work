package com.douding.server.util;

import com.douding.server.pojo.Admin;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author: ding dou
 * @className: AdminUtil
 * @packageName: com.douding.server.util
 * @description: 操作员工具类
 **/
public class AdminUtil {

    /**
     * @description:  获取当前登陆操作员
     *
     * @param
     * @return: com.douding.server.pojo.Admin
     * @title: getCurrentAdmin
     * @author: ding dou
     *
     */

    public static Admin getCurrentAdmin(){
        return (Admin) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

