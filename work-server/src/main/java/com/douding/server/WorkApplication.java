package com.douding.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ProjectName: work
 * @Package: com.douding.server
 * @ClassName: WorkApplication
 * @Author: LZB
 * @Description: SpringBoot启动类
 */

@SpringBootApplication
@MapperScan("com.douding.server.mapper")
public class WorkApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkApplication.class, args);
    }
}
