package com.douding.server.controller;

import com.douding.server.pojo.Admin;
import com.douding.server.service.IAdminService;
import com.douding.server.vo.AdminLogin;
import com.douding.server.vo.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author: ding dou
 * @className: LoginController
 * @packageName: com.douding.server.controller
 * @description: 登陆
 **/
@RestController
@Api(tags = "LoginController")
public class LoginController {
    @Autowired
    private IAdminService adminService;

    @ApiOperation(value = "登录后返回token")
    @PostMapping("/login")
    public RespBean login(@RequestBody  AdminLogin adminLogin, HttpServletRequest request){

        return adminService.login(adminLogin.getUsername(),adminLogin.getPassword(),adminLogin.getCode(),request);

    }

    @ApiOperation(value = "退出登录")
    @PostMapping("/logout")
    public RespBean logOut(){
        return RespBean.success("注销成功！");
    }


    @ApiOperation(value = "获取当前登录信息")
    @GetMapping("/admin/info")
    public Admin getAdminInfo(Principal principal){
        if(null==principal){
            return null;
        }
        String username=principal.getName();
        Admin admin=adminService.getAdminByUserName(username);
        admin.setPassword(null);
        admin.setRoles(adminService.getRoles(admin.getId()));
        return admin;
    }

}

