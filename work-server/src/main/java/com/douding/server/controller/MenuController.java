package com.douding.server.controller;


import com.douding.server.pojo.Menu;
import com.douding.server.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/sys/cfg")
public class MenuController {
    @Autowired
    private IMenuService menuService;


     @ApiOperation(value = "通过用户id查询菜单列表")
     @GetMapping("/menu")
    public List<Menu> getMenusByAdminId(){
         return menuService.getMenusByAdminId();
     }
}
