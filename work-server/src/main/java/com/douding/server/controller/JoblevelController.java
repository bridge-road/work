package com.douding.server.controller;


import com.douding.server.pojo.Joblevel;
import com.douding.server.service.IJoblevelService;
import com.douding.server.vo.RespBean;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/system/basic/joblevel")
public class JoblevelController {
    @Autowired
    private IJoblevelService joblevelService;

    @ApiOperation("获得所有职称")
    @GetMapping("/")
    public List<Joblevel> getAllJobLevels(){
        return joblevelService.list();
    }
    @ApiOperation("添加职称")
    @PostMapping("/")
    public RespBean addJobLevel(@RequestBody Joblevel joblevel){
        if(joblevelService.save(joblevel)){
            return RespBean.success("添加成功！！！");
        }
        return RespBean.error("添加失败");
    }

    @ApiOperation("更新职称")
    @PutMapping("/")
    public RespBean updataJobLevel(@RequestBody Joblevel joblevel){
        if(joblevelService.updateById(joblevel)){
            return RespBean.success("更新成功");
        }
        return RespBean.error("更新失败");
    }
    @ApiOperation("删除职称")
    @DeleteMapping("/{id}")
    public RespBean deleJobLevel(@PathVariable("id") Integer id){
        if(joblevelService.removeById(id)){
            return RespBean.success("删除成功！！");
        }
        return RespBean.error("删除失败！！！");
    }
    @ApiOperation("批量删除")
    @DeleteMapping("/")
    public RespBean deleJobLevels(Joblevel[] ids){
        if(joblevelService.removeByIds(Arrays.asList(ids))){
            return RespBean.success("批量输出成功！！！");
        }
        return RespBean.error("批量删除失败");
    }

}
