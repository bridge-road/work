package com.douding.server.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/employee-remove")
public class EmployeeRemoveController {

}
