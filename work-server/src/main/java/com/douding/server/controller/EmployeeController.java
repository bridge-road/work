package com.douding.server.controller;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.douding.server.pojo.*;
import com.douding.server.service.*;
import com.douding.server.vo.ResPageBean;
import com.douding.server.vo.RespBean;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author douding
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/employee/basic")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IJoblevelService joblevelService;
    @Autowired
    private INationService nationService;
    @Autowired
    private IPoliticsStatusService politicsStatusService;
    @Autowired
    private IPositionService positionService;

    @ApiOperation(value = "获取所有员工(分页)")
    @GetMapping("/")
    public ResPageBean getEmployee(@RequestParam(defaultValue = "1") Integer currentPage, @RequestParam(defaultValue
            = "10") Integer size, Employee employee, LocalDate[] beginDateScope) {
        return employeeService.getEmployeeByPage(currentPage, size, employee, beginDateScope);
    }

    @ApiOperation("获取所有部门")
    @GetMapping("/department")
    public List<Department> getAllDepartment() {
        return departmentService.getAllDepartments();
    }

    @ApiOperation("获取所有职称")
    @GetMapping("/level")
    public List<Joblevel> getAllJobLevel() {
        return joblevelService.list();
    }

    @ApiOperation("获取工号")
    @GetMapping("/workId/{id}")
    public RespBean getWorkId(@PathVariable("id") Integer id) {
        Employee employee = employeeService.getById(id);
        if (StringUtils.isEmpty(employee)) {
            return RespBean.success(null, employee.getIdCard());
        }
        return RespBean.error("查询失败！！");
    }

    @ApiOperation("获取所有民族")
    @GetMapping("/nation")
    public List<Nation> getAllNations() {
        return nationService.list();
    }

    @ApiOperation("获取所有政治面貌")
    @GetMapping("/status")
    public List<PoliticsStatus> getAllPolitics() {
        return politicsStatusService.list();
    }

    @ApiOperation("获得所有职位")
    @GetMapping("/position")
    public List<Position> getAllPositions() {
        return positionService.list();
    }

    @ApiOperation("添加员工")
    @PostMapping("/add")
    public RespBean addEmployee(@RequestBody Employee employee) {
        return  employeeService.addEmp(employee);

    }

    @ApiOperation(value = "更新员工")
    @PutMapping("/")
    public RespBean updateEmp(@RequestBody Employee employee) {

        if (employeeService.updateById(employee)) {
            return RespBean.success("更新成功！！！");
        }
        return RespBean.error("更新失败！！！");
    }

    @ApiOperation("删除员工")
    @DeleteMapping("/{id}")
    public RespBean deleEmp(@PathVariable("id") Integer id) {
        if (employeeService.removeById(id)) {
            return RespBean.success("删除成功！！！");

        }
        return RespBean.error("删除失败！！！");
    }

    @ApiOperation("导出员工数据")
    @GetMapping(value = "/export",produces = "application/octet-stream")
    public void exportEmployee(HttpServletResponse response){
        List<Employee> employee = employeeService.getEmployee(null);
        ExportParams params=new ExportParams("员工表","员工表", ExcelType.HSSF);
        Workbook workbook = ExcelExportUtil.exportExcel(params, Employee.class, employee);
        ServletOutputStream outputStream=null;
        try {
            response.setHeader("content-type", "application/octet-stream");
            response.setHeader("content-disposition", "attachment;filename="+ URLEncoder.encode("员工表.xls","UTF-8" ));
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(null!=outputStream){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @ApiOperation(value = "导入员工数据")
    @PostMapping("/import")
    public RespBean importEmployee(MultipartFile file){
        ImportParams params=new ImportParams();
        //去掉标题行
        params.setTitleRows(1);
        List<Nation> nationList = nationService.list();

        List<PoliticsStatus> politicsStatuses = politicsStatusService.list();

        try {
            List<Employee> list = ExcelImportUtil.importExcel(file.getInputStream(), Employee.class, params);
            list.forEach(employee -> {
                employee.setNationId(nationList.get( nationList.indexOf( new Nation(employee.getNation().getName()))).getId());

                employee.setPoliticId(politicsStatuses.get(politicsStatuses.indexOf(new PoliticsStatus(employee.getPoliticsStatus().getName()))).getId());

            });
if(employeeService.saveBatch(list)){
    return  RespBean.success("导入成功！！！");
}

        } catch (Exception e) {
            e.printStackTrace();
        }
        return RespBean.error("导入失败！！！");
    }
}
