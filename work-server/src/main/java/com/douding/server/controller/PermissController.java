package com.douding.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.douding.server.pojo.Menu;
import com.douding.server.pojo.MenuRole;
import com.douding.server.pojo.Role;
import com.douding.server.service.IMenuRoleService;
import com.douding.server.service.IMenuService;
import com.douding.server.service.IRoleService;
import com.douding.server.vo.RespBean;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: ding dou
 * @className: PermissController
 * @packageName: com.douding.server.controller
 * @description: 权限组
 **/
@RestController
@RequestMapping("/system/basic/permiss")
public class PermissController {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IMenuService menuService;
    @Autowired
    private IMenuRoleService menuRoleService;

    @ApiOperation(value = "获得所有角色")
    @GetMapping("/")
    public List<Role> getAllRoles(){
        return roleService.list();
    }
    @ApiOperation("添加角色")
    @PostMapping("/role")
    public RespBean addRole(@RequestBody Role role){
        if (!role.getName().startsWith("ROLE_")){
            role.setName("ROLE_"+role.getName());
        }
        if (roleService.save(role)) {
            return RespBean.success("添加成功");
        }

        return RespBean.error("添加失败");
    }
    @ApiOperation(value = "删除角色")
    @DeleteMapping("/role/{rid}")
    public RespBean deleRole(@PathVariable("rid") Integer rid){
        if (roleService.removeById(rid)) {
            return RespBean.success("删除成功！！！");
        }
        return RespBean.error("删除失败！！！");
    }

    @ApiOperation(value = "查询所有菜单")
    @GetMapping("/menus")
    public List<Menu> getAllMenus(){
        return menuService.getAllMenus();
    }

    @ApiOperation("根据角色id查询菜单")
    @GetMapping("/mid/{rid}")
    public List<Integer> getMidByRid(@PathVariable("rid") Integer rid){
        return menuRoleService.list(new QueryWrapper<MenuRole>().eq("rid",rid)).stream()
                .map(MenuRole::getMid).collect(Collectors.toList());
    }
    @ApiOperation("更新角色菜单")
    @PutMapping("/")
    public RespBean updataMenuRole(Integer rid,Integer[] mids){
         return menuRoleService.updataMenusRole(rid,mids);
    }

}

