package com.douding.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: work
 * @Package: com.douding.server.vo
 * @ClassName: RespBean
 * @Author: Dingdou
 * @Description: 公共返回对象
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespBean {
    private long code;
    private String message;
    private Object object;

    /**
     * 功能描述 :成功返回对象
     * @author LZB
     * @param
     * @return
     */
    public static RespBean success(String message){
        return new RespBean(200,message,null);
    }
    /**
     * 功能描述 :成功返回结果
     * @author LZB
     * @param
     * @return
     */
    public  static RespBean success(String message,Object o){
        return new RespBean(200, message, o);
    }
    /**
     * 功能描述 :失败返回结果
     * @author LZB
     * @param
     * @return
     */
    public static RespBean error(String message){
        return new RespBean(500, message, null);
    }
    /**
     * @title: error
     * @description: 失败返回结果
     * @author: user
     * @param: [message, o]
     * @return: com.douding.server.vo.RespBean
     * @throws:
     */
    public static RespBean error(String message,Object o){

        return new RespBean(500, message, o);
    }


}
