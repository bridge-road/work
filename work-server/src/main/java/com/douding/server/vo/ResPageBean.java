package com.douding.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author: ding dou
 * @className: ResPageBean
 * @packageName: com.douding.server.vo
 * @description: 分页公共返回对象
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResPageBean {

    /**
     * 总条数
     */   
    private Long total;

    /**
     * 数据List
     */
    private List<?>data;
}

